#! /usr/bin/env python
import os
from subprocess import Popen, PIPE

for file in os.listdir("/vagrant"):
  if file.startswith("site-"):
      name = file[len("site-"):]
      if not os.path.exists("/vagrant/zotonic/priv/sites/" + name):
          os.symlink("/vagrant/" + file, "/vagrant/zotonic/priv/sites/" + name)
      p = Popen(["sudo", "-u", "postgres", "psql"], stdin=PIPE)
      p.communicate(input= ("\c zotonic\n"
                            "DO $$ BEGIN IF NOT EXISTS(SELECT schema_name FROM information_schema.schemata WHERE schema_name = '"
                            + name + "') THEN EXECUTE 'CREATE SCHEMA " + name +"'; END IF; END $$;"
                            "ALTER SCHEMA " + name + " OWNER TO zotonic;\n"))

for file in os.listdir("/vagrant/modules"):
    if file.startswith("mod_"):
        if not os.path.exists("/vagrant/zotonic/priv/modules/" + file):
            os.symlink("/vagrant/modules/" + file, "/vagrant/zotonic/priv/modules/" + file)
