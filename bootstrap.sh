#!/bin/bash

#sudo echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt precise main universe' > /etc/apt/sources.list
#sudo echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt precise-updates main universe' >> /etc/apt/sources.list
echo 'deb http://ftp.halifax.rwth-aachen.de/ubuntu/ precise main universe' > /etc/apt/sources.list
echo 'deb http://ftp.halifax.rwth-aachen.de/ubuntu/ precise-updates main universe' >> /etc/apt/sources.list


# install packages
export DEBIAN_FRONTEND=noninteractive
apt-get -y update
apt-get -y install python-software-properties
add-apt-repository -y ppa:markdlavin/erlang
apt-get -y update
#apt-get -y upgrade
apt-get -y install \
    make git curl build-essential \
    imagemagick postgresql-9.1 postgresql-client-9.1 erlang-nox inotify-tools
apt-get -y clean

echo 'export PATH=$PATH:/vagrant/zotonic/bin:/vagrant' >> /home/vagrant/.bashrc

cp /vagrant/sassc /usr/bin
ln -s /usr/bin/sassc /usr/bin/sass

# setup database
echo 'Check for zotonic database user'
echo \\dg | sudo -u postgres psql | grep zotonic
if [ 0 -ne $? ]; then
    cat | sudo -u postgres psql <<EOF
CREATE USER zotonic with password 'zotonic';
EOF
fi

echo 'Check for zotonic database'
echo \\l | sudo -u postgres psql | grep zotonic
if [ 0 -ne $? ]; then
    cat | sudo -u postgres psql <<EOF
CREATE DATABASE zotonic WITH
OWNER zotonic
ENCODING 'UTF-8'
LC_CTYPE 'en_US.utf8'
LC_COLLATE 'en_US.utf8'
TEMPLATE template0;
EOF
fi

# cloning and building zotonic
if [ ! -d /vagrant/zotonic ]; then
    cd /vagrant
    git clone https://github.com/hanikesn/zotonic.git
    cd zotonic
    git remote add upstream git@github.com:zotonic/zotonic.git
    echo '[{password,"admin"}, {listen_ip,any}, {modify_date,{{2013,11,9},{23,45,50}}}, {listen_port,8000}].' > /vagrant/zotonic/priv/config
fi

/vagrant/update.py

# now build it...
cd /vagrant/zotonic
make

ZOTONIC_BIN=/vagrant/zotonic/bin/zotonic

chown vagrant:vagrant /vagrant/zotonic -R

# and start!
sudo -u vagrant -i $ZOTONIC_BIN start

echo
echo 'Visit http://local.zeitmacht.com:8000 on the host machine. Lets go!'
echo
